﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StockDemo.Services.Helpers
{
    public static class PerformanceCalculatorHelper
    {
        public static decimal? CalculateDifference(decimal? refValue, decimal? actualValue)
        {
            if (!refValue.HasValue || !actualValue.HasValue)
            {
                return null;
            }

            return (actualValue.Value - refValue.Value) / Math.Abs(refValue.Value) * 100;
        }
    }
}
