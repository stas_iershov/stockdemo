import React from 'react';
import { CartesianGrid, XAxis, YAxis, Tooltip, Area, AreaChart, Legend } from 'recharts';

import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

class SymbolChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      symbol: '',
      data: {},
      loadedSymbol: '',
      dataLoaded: false,
      intradayDataLoaded: false,
      intradayData: {},
      comparisonData: [],
      intradayDate: new Date(),
    };

    this.getSymbolData = this.getSymbolData.bind(this);
    this.getIntradateData = this.getIntradateData.bind(this);
    this.setSymbol = this.setSymbol.bind(this);
    this.setIntradayDate = this.setIntradayDate.bind(this);
  }

  getSymbolData() {
    const headers = { 'Content-Type': 'application/json' };

    var symbol = this.state.symbol;
    fetch(`https://localhost:44309/api/v1/stockdata/${symbol}`, { headers })
      .then(function (response) {
        console.log(response.status);
        if (!response.ok) {
          throw new Error('HTTP status ' + response.status);
        }
        return response.json();
      })
      .then((data) => {
        console.log(data);
        let comparison = [];
        data.symbolPerformance.entries.forEach((element, index) => {
          comparison.push({
            symbol: element.relativePerformance,
            reference: data.referenceSymbolPerformance.entries[index].relativePerformance,
            date: element.dateTime,
          });
        });
        this.setState({
          data: data,
          dataLoaded: true,
          comparisonData: comparison,
          loadedSymbol: symbol,
          intradayDate: new Date(data.symbolPerformance.entries[0].dateTime),
        });
      })
      .catch(function () {
        alert('Could not load the data');
      });
  }

  getIntradateData() {
    const headers = { 'Content-Type': 'application/json' };

    var symbol = this.state.symbol;
    var date =
      this.state.intradayDate.getFullYear() +
      '-' +
      (this.state.intradayDate.getMonth() + 1) +
      '-' +
      this.state.intradayDate.getDate();
    console.log('date', date);
    fetch(`https://localhost:44309/api/v1/stockdata/${symbol}/${date}`, { headers })
      .then(function (response) {
        console.log(response.status);
        if (!response.ok) {
          throw new Error('HTTP status ' + response.status);
        }
        return response.json();
      })
      .then((data) => {
        console.log(data);
        this.setState({
          intradayData: data,
          intradayDataLoaded: true,
        });
      })
      .catch(function () {
        alert('Could not load the data');
      });
  }

  setSymbol(e) {
    this.setState({ symbol: e.target.value });
  }

  setIntradayDate(date) {
    this.setState({ intradayDate: date });
  }

  formatXAxis(tickItem) {
    return new Date(tickItem).getDate();
  }

  formatXAxisTime(tickItem) {
    var date = new Date(tickItem);
    return `${date.getHours()}: ${date.getMinutes()}`;
  }

  formatYAxis(tickItem) {
    return tickItem + '%';
  }

  render() {
    return (
      <div>
        <input onChange={this.setSymbol} placeholder="symbol" type="text"></input>
        <input onClick={this.getSymbolData} type="button" value="Get symbol data" />

        {this.state.dataLoaded && (
          <div className="chart-container">
            <h3 className="chart-caption"> {this.state.loadedSymbol} vs SPY Comparison:</h3>
            <AreaChart width={400} height={400} label="test" data={this.state.comparisonData}>
              <defs>
                <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
                </linearGradient>
                <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
                </linearGradient>
              </defs>

              <Area
                type="monotone"
                name={this.state.loadedSymbol}
                dataKey="symbol"
                stroke="#8884d8"
                fillOpacity={1}
                fill="url(#colorUv)"
              />
              <Area
                name="SPY"
                type="monotone"
                dataKey="reference"
                stroke="#8884d8"
                fillOpacity={1}
                fill="url(#colorPv)"
              />
              <CartesianGrid stroke="#ccc" />
              <XAxis dataKey="date" tickFormatter={this.formatXAxis} />
              <YAxis dataKey="symbol" tickFormatter={this.formatYAxis} />
              <Legend />
              <Tooltip />
            </AreaChart>

            <h3 className="chart-caption"> {this.state.loadedSymbol} weekly price:</h3>
            <AreaChart width={400} height={400} label="test" data={this.state.data.symbolPerformance.entries}>
              <defs>
                <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
                </linearGradient>
                <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
                </linearGradient>
              </defs>

              <Area type="monotone" dataKey="price" stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)" />
              <CartesianGrid stroke="#ccc" />
              <XAxis dataKey="dateTime" tickFormatter={this.formatXAxis} />
              <YAxis domain={['auto', 'auto']} dataKey="price" />

              <Tooltip />
            </AreaChart>

            <h3 className="chart-caption"> SPY weekly price:</h3>
            <AreaChart width={400} height={400} label="test" data={this.state.data.referenceSymbolPerformance.entries}>
              <defs>
                <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
                </linearGradient>
                <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
                </linearGradient>
              </defs>

              <Area type="monotone" dataKey="price" stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)" />
              <CartesianGrid stroke="#ccc" />
              <XAxis dataKey="dateTime" tickFormatter={this.formatXAxis} />
              <YAxis domain={['auto', 'auto']} dataKey="price" />

              <Tooltip />
            </AreaChart>
          </div>
        )}

        {this.state.dataLoaded && (
          <div>
            <DatePicker selected={this.state.intradayDate} onChange={(date) => this.setIntradayDate(date)} />
            <input onClick={this.getIntradateData} type="button" value="Get intraday data" />
            <br></br>
          </div>
        )}

        {this.state.intradayDataLoaded && (
          <div className="chart-container">
            <h3 className="chart-caption"> Intraday performance:</h3>
            <span>(Local time)</span>
            <AreaChart width={400} height={400} label="test" data={this.state.intradayData.symbolPerformance.entries}>
              <defs>
                <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
                </linearGradient>
                <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
                </linearGradient>
              </defs>

              <Area
                type="monotone"
                dataKey="relativePerformance"
                stroke="#8884d8"
                fillOpacity={1}
                fill="url(#colorUv)"
              />
              <CartesianGrid stroke="#ccc" />
              <XAxis dataKey="dateTime" tickFormatter={this.formatXAxisTime} />
              <YAxis domain={['auto', 'auto']} dataKey="relativePerformance" />

              <Tooltip />
            </AreaChart>
          </div>
        )}
      </div>
    );
  }
}

export default SymbolChart;
