﻿using System;

namespace StockDemo.Data.StockDataDtos
{
    public class SymbolDataRequestDto
    {
        public string Symbol { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
