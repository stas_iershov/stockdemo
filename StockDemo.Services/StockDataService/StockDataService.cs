﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StockDemo.Data;
using StockDemo.Data.Entities;
using StockDemo.Data.StockDataDtos;

namespace StockDemo.Services.StockDataService
{
    public class StockDataService : IStockDataService
    {

        private readonly StockDemoContext _stockDemoContext;
        private const int MinDailyEntries = 6;

        public StockDataService(StockDemoContext stockDemoContext)
        {
            _stockDemoContext = stockDemoContext;
        }

        public async Task<(bool success, SymbolDataPerformanceDto stockData)> GetWeeklyStockData(SymbolDataRequestDto requestDto)
        {
            var entries =  await _stockDemoContext.StockDataEntries.Where(x =>
                x.Symbol == requestDto.Symbol &&
                x.DateTime.Date >= requestDto.StartDate && x.DateTime.Date <= requestDto.EndDate).ToListAsync();

            if (entries.Any())
            {
                var result = GetSymbolDataPerformanceDtoResult(requestDto, entries);
                return (true, result);
            }

            return (false, null);
        }


        public async Task<(bool success, SymbolDataPerformanceDto stockData)> GetIntradayStockData(SymbolDataRequestDto requestDto)
        {
            var entries = await _stockDemoContext.StockDataEntries.Where(x =>
                x.Symbol == requestDto.Symbol &&
                x.DateTime.Date == requestDto.StartDate).ToListAsync();

            if (entries.Count > MinDailyEntries)
            {
                var result = GetSymbolDataPerformanceDtoResult(requestDto, entries);
                return (true, result);
            }

            return (false, null);
        }

        public async Task SaveStockData(SymbolDataPerformanceDto saveSymbolDataModel)
        {
            var entriesToPut = new List<StockDataEntry>(saveSymbolDataModel.SymbolPriceQuote.Count);

            foreach (var dailyData in saveSymbolDataModel.SymbolPriceQuote.Where(x => x.MarketPrice.HasValue))
            {
                var entry = new StockDataEntry()
                {
                    Symbol = saveSymbolDataModel.Symbol,
                    DateTime = dailyData.DateTime,
                    Price = dailyData.MarketPrice.Value
                };

                entriesToPut.Add(entry);
            }


            await _stockDemoContext.StockDataEntries.AddRangeAsync(entriesToPut);
            await _stockDemoContext.SaveChangesAsync();
        }


        private static SymbolDataPerformanceDto GetSymbolDataPerformanceDtoResult(SymbolDataRequestDto requestDto, List<StockDataEntry> entries)
        {
            var result = new SymbolDataPerformanceDto()
            {
                Symbol = requestDto.Symbol,
                SymbolPriceQuote = new List<StockDataPriceDto>(entries.Count)
            };

            foreach (var stockDataEntry in entries)
            {
                result.SymbolPriceQuote.Add(new StockDataPriceDto()
                {
                    DateTime = stockDataEntry.DateTime,
                    MarketPrice = stockDataEntry.Price
                });
            }

            return result;
        }
    }
}
