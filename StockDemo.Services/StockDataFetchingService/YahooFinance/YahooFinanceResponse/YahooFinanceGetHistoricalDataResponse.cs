﻿using System.Collections.Generic;

namespace StockDemo.Services.StockDataFetchingService.YahooFinance.YahooFinanceResponse
{
    public class YahooFinanceGetHistoricalDataResponse
    {
        public List<Price> Prices { get; set; }
        public bool IsPending { get; set; }
        public int FirstTradeDate { get; set; }
        public string Id { get; set; }
        public TimeZone TimeZone { get; set; }
        public List<object> EventsData { get; set; }
    }

    public class Price
    {
        public int Date { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public int Volume { get; set; }
        public decimal Adjclose { get; set; }
    }

    public class TimeZone
    {
        public int GmtOffset { get; set; }
    }

}
