﻿using System;
using System.Collections.Generic;


namespace StockDemo.WebApi.ViewModels
{
    public class SymbolComparisonViewModel
    {
        public SymbolPerformancePerformanceViewModel SymbolPerformance { get; set; }
        public SymbolPerformancePerformanceViewModel ReferenceSymbolPerformance { get; set; }
    }

    public class PortfolioSymbolComparisonViewModel
    {
        public List<SymbolPerformancePerformanceViewModel> SymbolsPerformance { get; set; }
        public SymbolPerformancePerformanceViewModel StockPortfolioSymbolsPerformance { get; set; }
        public SymbolPerformancePerformanceViewModel ReferenceSymbolPerformance { get; set; }
    }

    public class SymbolPerformancePerformanceViewModel
    {
        public string Symbol { get; set; }
        public List<SymbolPerformanceEntry> Entries { get; set; }
    }

    public class SymbolPerformanceEntry
    {
        public decimal? Price { get; set; }
        public DateTime DateTime { get; set; }
        public decimal? RelativePerformance { get; set; }
    }
}
