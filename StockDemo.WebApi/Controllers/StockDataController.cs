﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StockDemo.Data.StockDataDtos;
using StockDemo.Services.Helpers;
using StockDemo.Services.StockDataFetchingService;
using StockDemo.Services.StockDataService;
using StockDemo.WebApi.ViewModels;

namespace StockDemo.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class StockDataController : ControllerBase
    {
        private const string ReferenceSymbol = "SPY";
        private readonly IStockDataFetchingService _dataFetchingService;
        private readonly IStockDataService _stockDataService;

        public StockDataController(IStockDataFetchingService dataFetchingService, IStockDataService stockDataService)
        {
            _dataFetchingService = dataFetchingService;
            _stockDataService = stockDataService;
        }

        [HttpGet("{symbol}")]
        public async Task<SymbolComparisonViewModel> GetWeekly(string symbol)
        {
            var symbolData = await GetWeeklySymbolData(symbol);
            var referenceSymbolData = await GetWeeklySymbolData(ReferenceSymbol);

            var symbolFirstDayPrice = symbolData.SymbolPriceQuote.OrderBy(x => x.DateTime).First().MarketPrice;
            var referenceSymbolFirstDayPrice = referenceSymbolData.SymbolPriceQuote.OrderBy(x => x.DateTime).First().MarketPrice;

            var result = new SymbolComparisonViewModel()
            {
                SymbolPerformance = new SymbolPerformancePerformanceViewModel()
                {
                    Symbol = symbolData.Symbol,
                    Entries = ConvertStockDataDtoToViewModel(symbolFirstDayPrice, symbolData.SymbolPriceQuote)
                },

                ReferenceSymbolPerformance = new SymbolPerformancePerformanceViewModel()
                {
                    Symbol = ReferenceSymbol,
                    Entries = ConvertStockDataDtoToViewModel(referenceSymbolFirstDayPrice, referenceSymbolData.SymbolPriceQuote)
                }
            };

            return result;
        }


        [HttpGet("portfolio/{symbols}/{date}")]
        public async Task<PortfolioSymbolComparisonViewModel> GetPortfolio(string symbols, DateTime date)
        {
            var symbolsToGet = symbols.Split(",");

            var referenceSymbolIntradayData = await GetIntradaySymbolData(ReferenceSymbol, date, true);
            var referenceSymbolFirstHourPrice = referenceSymbolIntradayData.SymbolPriceQuote.OrderBy(x => x.DateTime).First().MarketPrice;

            var result = new PortfolioSymbolComparisonViewModel
            {
                SymbolsPerformance = new List<SymbolPerformancePerformanceViewModel>(),
                StockPortfolioSymbolsPerformance = new SymbolPerformancePerformanceViewModel()
                {
                    Entries = new List<SymbolPerformanceEntry>()
                },
                ReferenceSymbolPerformance = new SymbolPerformancePerformanceViewModel()
                {
                    Symbol = ReferenceSymbol,
                    Entries = ConvertStockDataDtoToViewModel(referenceSymbolFirstHourPrice,
                        referenceSymbolIntradayData.SymbolPriceQuote)
                }
            };



            foreach (var symbol in symbolsToGet)
            {
                var symbolIntradayData = await GetIntradaySymbolData(symbol, date, true);
                var symbolFirstHourPrice = symbolIntradayData.SymbolPriceQuote.OrderBy(x => x.DateTime).First().MarketPrice;
                result.SymbolsPerformance.Add(new SymbolPerformancePerformanceViewModel()
                {
                    Entries = ConvertStockDataDtoToViewModel(symbolFirstHourPrice, symbolIntradayData.SymbolPriceQuote),
                    Symbol = symbol
                });
            }
            
            var allSymbolsPerformance = result.SymbolsPerformance.SelectMany(x => x.Entries).GroupBy(x => x.DateTime).ToList();

            result.StockPortfolioSymbolsPerformance = new SymbolPerformancePerformanceViewModel()
            {
                Symbol = symbols,
                Entries = new List<SymbolPerformanceEntry>()
            };

            foreach (var performance in allSymbolsPerformance)
            {
                decimal averagePrice = 0;
                
                var cumulativeEntry = new SymbolPerformanceEntry()
                {
                    DateTime = performance.Key,
                };

                foreach (var symbolPerformanceEntry in performance)
                {
                    if (symbolPerformanceEntry.Price.HasValue)
                    {
                        averagePrice = averagePrice + symbolPerformanceEntry.Price.Value;
                    }
                }

                cumulativeEntry.Price = averagePrice / performance.Count();

                result.StockPortfolioSymbolsPerformance.Entries.Add(cumulativeEntry);
            }

            var firstHourPerformance = result.StockPortfolioSymbolsPerformance.Entries.OrderBy(x => x.DateTime)
                .FirstOrDefault().Price;

            foreach (var symbolPerformanceEntry in result.StockPortfolioSymbolsPerformance.Entries)
            {
                symbolPerformanceEntry.RelativePerformance =
                    PerformanceCalculatorHelper.CalculateDifference(firstHourPerformance, symbolPerformanceEntry.Price);
            }


            return result;
        }


        [HttpGet("{symbol}/{date}")]
        public async Task<SymbolComparisonViewModel> GetDaily(string symbol, DateTime date)
        {

            var symbolIntradayData = await GetIntradaySymbolData(symbol, date);
            var referenceSymbolIntradayData = await GetIntradaySymbolData(ReferenceSymbol, date);

            var symbolFirstHourPrice = symbolIntradayData.SymbolPriceQuote.OrderBy(x => x.DateTime).First().MarketPrice;
            var referenceSymbolFirstHourPrice = referenceSymbolIntradayData.SymbolPriceQuote.OrderBy(x => x.DateTime).First().MarketPrice;

            var result = new SymbolComparisonViewModel()
            {
                SymbolPerformance = new SymbolPerformancePerformanceViewModel()
                {
                    Symbol = symbolIntradayData.Symbol,
                    Entries = ConvertStockDataDtoToViewModel(symbolFirstHourPrice, symbolIntradayData.SymbolPriceQuote)
                },

                ReferenceSymbolPerformance = new SymbolPerformancePerformanceViewModel()
                {
                    Symbol = ReferenceSymbol,
                    Entries = ConvertStockDataDtoToViewModel(referenceSymbolFirstHourPrice, referenceSymbolIntradayData.SymbolPriceQuote)
                }
            };

            return result;
        }

        private static List<SymbolPerformanceEntry> ConvertStockDataDtoToViewModel(decimal? symbolFirstDayPrice, List<StockDataPriceDto> stockData)
        {
            return stockData.Select(x => new SymbolPerformanceEntry()
            {
                DateTime = x.DateTime,
                Price = x.MarketPrice,
                RelativePerformance =
                    PerformanceCalculatorHelper.CalculateDifference(symbolFirstDayPrice, x.MarketPrice)
            }).ToList();
        }

        private async Task<SymbolDataPerformanceDto> GetWeeklySymbolData(string symbol)
        {
            var startDate = DateTimeHelper.GetLastWeekMonday(DateTime.UtcNow).Date;
            var requestDataModel = new SymbolDataRequestDto()
            {
                StartDate = startDate,
                EndDate = startDate.AddDays(5),
                Symbol = symbol
            };

            var dbRequestResult = await _stockDataService.GetWeeklyStockData(requestDataModel);

            if (dbRequestResult.success)
            {
                //Should always be 5, otherwise data consistency is not guaranteed and data provider should be used
                if (dbRequestResult.stockData.SymbolPriceQuote.Count == 5)
                {
                    return dbRequestResult.stockData;
                }
            }

            var result = await _dataFetchingService.GetWeeklySymbolPerformance(requestDataModel);

            //In a prod app IHostedService background task should be run here to improve performance
            await _stockDataService.SaveStockData(result);
            return result;
        }

        private async Task<SymbolDataPerformanceDto> GetIntradaySymbolData(string symbol, DateTime targetDate, bool twoHoursInterval = false)
        {
            var requestDataModel = new SymbolDataRequestDto()
            {
                Symbol = symbol,
                StartDate = targetDate.Date,
                TwoHoursInterval = twoHoursInterval
            };

            var dbRequestResult = await _stockDataService.GetIntradayStockData(requestDataModel);

            if (dbRequestResult.success)
            {
                return dbRequestResult.stockData;
            }

            var result = await _dataFetchingService.GetIntradaySymbolPerformance(requestDataModel);

            if (twoHoursInterval)
            {
                var filteredResult = new SymbolDataPerformanceDto()
                {
                    Symbol = symbol,
                    SymbolPriceQuote = new List<StockDataPriceDto>()
                };

                for (int i = 0; i < result.SymbolPriceQuote.Count; i++)
                {
                    var currentEntry = result.SymbolPriceQuote[i];
                    if (i == 0)
                    {
                        filteredResult.SymbolPriceQuote.Add(currentEntry);
                    }
                    else
                    {
                        var spanDifference = currentEntry.DateTime - filteredResult.SymbolPriceQuote.Last().DateTime;
                        if (spanDifference > TimeSpan.FromHours(1))
                        {
                            filteredResult.SymbolPriceQuote.Add(currentEntry);
                        }
                    }
                }

                return filteredResult;
            }

            return result;
        }
    }
}
