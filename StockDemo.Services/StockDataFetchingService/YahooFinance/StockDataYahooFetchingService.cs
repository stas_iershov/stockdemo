﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using StockDemo.Data.StockDataDtos;
using StockDemo.Services.StockDataFetchingService.YahooFinance.YahooFinanceResponse;

namespace StockDemo.Services.StockDataFetchingService.YahooFinance
{
    public class StockDataYahooFetchingService : IStockDataFetchingService
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        private readonly IConfigurationSection _configurationSection;
        private readonly string _endpoint;
        private readonly string _host;
        private readonly string _key;

        public StockDataYahooFetchingService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _configurationSection = _configuration.GetSection("StockDataEndpoints").GetSection("YahooFinance");
            _endpoint = _configurationSection["YahooFinanceUri"];
            _host = _configurationSection["YahooFinanceHost"];
            _key = _configurationSection["YahooFinanceKey"];
        }

        public async Task<SymbolDataPerformanceDto> GetWeeklySymbolPerformance(SymbolDataRequestDto requestDto)
        {
            var uri = CreateGetHistoricalDataRequestUri(requestDto);
            var request = CreateYahooFinanceRequest(uri);

            var sendResult = await _httpClient.SendAsync(request);

            if (sendResult.IsSuccessStatusCode)
            {
                var resultString = await sendResult.Content.ReadAsStringAsync();

                try
                {
                    var result = JsonSerializer.Deserialize<YahooFinanceGetHistoricalDataResponse>(resultString, new JsonSerializerOptions()
                    {
                        PropertyNameCaseInsensitive = true
                    });

                    var convertedResult = ConvertYahooHistoryResponseToDto(result);
                    convertedResult.Symbol = requestDto.Symbol;
                    return convertedResult;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }

            //The problem should be logged and passed to exception here
            throw new ApplicationException("Error querying Yahoo Finance API");
        }


        public async Task<SymbolDataPerformanceDto> GetIntradaySymbolPerformance(SymbolDataRequestDto requestDto)
        {
            var uri = CreateGetIntradayDataRequestUri(requestDto);
            var request = CreateYahooFinanceRequest(uri);

            var sendResult = await _httpClient.SendAsync(request);

            if (sendResult.IsSuccessStatusCode)
            {
                var resultString = await sendResult.Content.ReadAsStringAsync();

                try
                {
                    
                    var result = JsonSerializer.Deserialize<YahooFinanceGetChartDataResponse>(resultString, new JsonSerializerOptions()
                    {
                        PropertyNameCaseInsensitive = true
                    });


                    //Assume that we don't want to cache all the data
                    var convertedResult = ConvertYahooChartResponseToDto(result, requestDto.StartDate);
                    convertedResult.Symbol = requestDto.Symbol;

                    return convertedResult;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }

            // The problem should be logged and passed to exception here
            throw new ApplicationException("Error querying Yahoo Finance API");
        }

        private HttpRequestMessage CreateYahooFinanceRequest(Uri uri)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            request.Headers.Add("x-rapidapi-host", _host);
            request.Headers.Add("x-rapidapi-key", _key);
            return request;
        }

        private Uri CreateGetHistoricalDataRequestUri(SymbolDataRequestDto requestDto)
        {
            var startPeriod = new DateTimeOffset(requestDto.StartDate).ToUnixTimeSeconds();
            var endPeriod = new DateTimeOffset(requestDto.EndDate).ToUnixTimeSeconds();

            // Not the nicest way to build the query string, but the most performant (compared to RestSharp and so on)
            var requestUri = new UriBuilder($"{_endpoint}{_configurationSection["GetHistoryEndpoint"]}");
            var queryBuilder = new StringBuilder();
            queryBuilder.Append("?period1=").Append(startPeriod);
            queryBuilder.Append("&period2=").Append(endPeriod);
            queryBuilder.Append("&symbol=").Append(requestDto.Symbol);
            requestUri.Query = queryBuilder.ToString();
            return requestUri.Uri;
        }

        private Uri CreateGetIntradayDataRequestUri(SymbolDataRequestDto requestDto)
        {
            var requestUri = new UriBuilder($"{_endpoint}{_configurationSection["GetChartEndpoint"]}");
            var queryBuilder = new StringBuilder();
            queryBuilder.Append("&symbol=").Append(requestDto.Symbol);
            queryBuilder.Append("&interval=").Append("60m");
            queryBuilder.Append("&region=").Append("US");
            queryBuilder.Append("&lang=").Append("en");
            queryBuilder.Append("&range=").Append("1mo");
            requestUri.Query = queryBuilder.ToString();
            return requestUri.Uri;
        }



        private static SymbolDataPerformanceDto ConvertYahooHistoryResponseToDto(YahooFinanceGetHistoricalDataResponse response)
        {
            var result = new SymbolDataPerformanceDto()
            {
                SymbolPriceQuote = new List<StockDataPriceDto>()
            };

            foreach (var price in response.Prices)
            {
                result.SymbolPriceQuote.Add(new StockDataPriceDto()
                {
                    MarketPrice = price.Close,
                    DateTime = DateTimeOffset.FromUnixTimeSeconds(price.Date).UtcDateTime
                });
            }

            return result;
        }


        private static SymbolDataPerformanceDto ConvertYahooChartResponseToDto(YahooFinanceGetChartDataResponse response, DateTime startDate)
        {
            var result = new SymbolDataPerformanceDto()
            {
                SymbolPriceQuote = new List<StockDataPriceDto>()
            };

            //Get start interval in EDT time:
            startDate = DateTime.SpecifyKind(startDate, DateTimeKind.Utc);

            var endDate = startDate.AddHours(16);
            startDate = startDate.AddHours(9).AddMinutes(30);
            
            startDate = startDate.AddSeconds(-response.Chart.Result[0].Meta.Gmtoffset);
            endDate = endDate.AddSeconds(-response.Chart.Result[0].Meta.Gmtoffset);
            var unixStartDate = new DateTimeOffset(startDate).ToUnixTimeSeconds();
            var unixEndDate = new DateTimeOffset(endDate).ToUnixTimeSeconds();

            var chartResult = response.Chart.Result[0];

            for (int i = 0; i < chartResult.Timestamp.Count; i++)
            {
                var timeStamp = chartResult.Timestamp[i];
                var targetDate = DateTimeOffset.FromUnixTimeSeconds(timeStamp).UtcDateTime;

                if (timeStamp >= unixStartDate && timeStamp <= unixEndDate)
                {
                    result.SymbolPriceQuote.Add(new StockDataPriceDto()
                    {
                        MarketPrice = chartResult.Indicators.Quote[0].Close[i],
                        DateTime = targetDate
                    });
                }
            }


            return result;
        }
    }
}
