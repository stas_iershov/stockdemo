﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StockDemo.Data.Migrations
{
    public partial class PriceFieldUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "StockDataEntries",
                type: "decimal(28,20)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(28)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "StockDataEntries",
                type: "decimal(28)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(28,20)");
        }
    }
}
