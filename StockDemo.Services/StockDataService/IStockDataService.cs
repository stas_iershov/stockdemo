﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using StockDemo.Data.StockDataDtos;

namespace StockDemo.Services.StockDataService
{
    public interface IStockDataService
    {
        Task SaveStockData(SymbolDataPerformanceDto saveSymbolDataModel);

        Task<(bool success, SymbolDataPerformanceDto stockData)> GetWeeklyStockData(
            SymbolDataRequestDto requestDto);

        Task<(bool success, SymbolDataPerformanceDto stockData)> GetIntradayStockData(
            SymbolDataRequestDto requestDataModel);
    }
}
