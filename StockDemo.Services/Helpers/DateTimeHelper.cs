﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StockDemo.Services.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime GetLastWeekMonday(DateTime currentDateTime)
        {
            return currentDateTime.AddDays(-(int)currentDateTime.DayOfWeek - 6);
        }
    }
}
