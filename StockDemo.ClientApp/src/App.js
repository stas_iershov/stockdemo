import React from 'react';
import './App.css';
import SymbolChart from './SymbolChart';

function App() {
  return (
    <div className="App">
      <SymbolChart />
    </div>
  );
}

export default App;
