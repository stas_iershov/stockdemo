using System;
using System.Globalization;
using StockDemo.Services.Helpers;
using Xunit;

namespace StockDemo.UnitTests
{
    public class DateTimeHelperTests
    {
        [Theory]
        [InlineData("8/7/2020", "7/27/2020")]
        [InlineData("3/9/2020", "3/2/2020")]
        [InlineData("8/9/2020", "8/3/2020")]
        public void DateTimeHelperReturnsProperFirstDay(string currentDayText, string expectedMondayText)
        {
            var currentDay = DateTime.Parse(currentDayText, CultureInfo.InvariantCulture);
            var expectedMonday = DateTime.Parse(expectedMondayText, CultureInfo.InvariantCulture);

            var firstDay = DateTimeHelper.GetLastWeekMonday(currentDay);
            Assert.Equal(firstDay, expectedMonday);
        }
    }
}
