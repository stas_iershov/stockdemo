# StockData demo

This app represents a prototype of a Web API returning stock symbols performance data. The prototype is fully working and allows users to consume stock symbols data and compare it with the reference symbol. There's also a simple React frontend app (decoupled from the API) which is able to consume the data and display some charts (weekly and intraday performance of symbols).

The API is intentionally simplified (considering the time frame) and contains some design desicions which wouldn't be used in a real life application, such as:

  - The used database is MS SQL (there's no need to store this kind of data in a relational database)
  - The repository is not abstracted (service layer is using db context directly)
  - Exception handling is simplified
  - There are no integration/unit tests and no logging

# Local deployment

The app doesn't depend on any specific libraries, so  `dotnet restore` and `npm install` for the frontend app is enough to get it up and runnning.

# API Endpoints

There are 2 endpoint

Last week symbol data:
```sh
GET: api/v1/stockdata/{symbol}/
e.g. api/v1/stockdata/MSFT/
```
Intraday symbol data:
```sh
GET: api/v1/stockdata/{symbol}/{date}/
e.g. api/v1/stockdata/MSFT/07-27-2020
```
  
# Response schema 
Both endpoints return the same following structure:
```json
{
   "symbolPerformance":{
      "symbol":"MSFT",
      "entries":[
         {
            "price": decimal,
            "dateTime": date,
            "relativePerformance":0% //shows the difference from the first day in %
         },
         {
            "price": decimal,
            "dateTime": date,
            "relativePerformance":-0.89% //shows the difference from the first day in %
         },
         ...
      ]
   },
   "referenceSymbolPerformance":{
      "symbol":"SPY",
      "entries":[
         {
            "price": decimal,
            "dateTime": date,
            "relativePerformance":0%
         },
         {
            "price":321.1700134277344,
            "dateTime":"2020-07-28T13:30:00",
            "relativePerformance":-0.63%
         },
         ...
      ]
   }
}
```
# Example of the simple React app
![picture](https://i.imgur.com/gr22TuP.png)




