﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using StockDemo.Data.Entities;

namespace StockDemo.Data
{
    public class StockDemoContext : DbContext
    {
        public DbSet<StockDataEntry> StockDataEntries { get; set; }

        public StockDemoContext(DbContextOptions<StockDemoContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StockDataEntry>()
                .HasKey(x => new { x.Symbol, x.DateTime });

            modelBuilder.Entity<StockDataEntry>()
                .HasIndex(nameof(StockDataEntry.Symbol), nameof(StockDataEntry.DateTime));
        }
    }
}
