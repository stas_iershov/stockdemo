﻿using System;
using System.Collections.Generic;

namespace StockDemo.Data.StockDataDtos
{
    public class SymbolDataPerformanceDto
    {
        public string Symbol { get; set; }
        public List<StockDataPriceDto> SymbolPriceQuote { get; set; }
    }

    public class StockDataPriceDto
    {
        public decimal? MarketPrice { get; set; }
        public DateTime DateTime { get; set; }
    }
}
