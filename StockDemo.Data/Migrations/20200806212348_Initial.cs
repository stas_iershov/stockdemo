﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StockDemo.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StockDataEntries",
                columns: table => new
                {
                    Symbol = table.Column<string>(nullable: false),
                    DateTime = table.Column<DateTime>(nullable: false),
                    Price = table.Column<decimal>(type: "decimal(28)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockDataEntries", x => new { x.Symbol, x.DateTime });
                });

            migrationBuilder.CreateIndex(
                name: "IX_StockDataEntries_Symbol_DateTime",
                table: "StockDataEntries",
                columns: new[] { "Symbol", "DateTime" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StockDataEntries");
        }
    }
}
