﻿using System;
using System.Collections.Generic;
using System.Text;
using StockDemo.Services.Helpers;
using Xunit;

namespace StockDemo.UnitTests
{
    public class PerformanceCalculatorTests
    {

        [Theory]
        [InlineData(100, 120, 20)]
        [InlineData(80, 100, 25)]
        [InlineData(135.124, 167.367, 23.86)]
        public void PerformanceCalculatorShouldReturnProperDifference(decimal refValue, decimal actualValue, decimal expectedDiff)
        {
            var difference = PerformanceCalculatorHelper.CalculateDifference(refValue, actualValue);
            Assert.Equal(difference.Value, expectedDiff, 2);
        }
    }
}
