﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using StockDemo.Data.StockDataDtos;

namespace StockDemo.Services.StockDataFetchingService
{
    public interface IStockDataFetchingService
    {
        Task<SymbolDataPerformanceDto> GetWeeklySymbolPerformance(SymbolDataRequestDto requestDto);
        Task<SymbolDataPerformanceDto> GetIntradaySymbolPerformance(SymbolDataRequestDto requestDto);
    }
}
