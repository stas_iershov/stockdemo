﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StockDemo.Data.Entities
{
    public class StockDataEntry
    {
        public string Symbol { get; set; }
        public DateTime DateTime { get; set; }
        [Column(TypeName = "decimal(28,20)")]
        public decimal Price { get; set; }
    }
}
